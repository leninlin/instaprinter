var request = require('request');
var qs = require('querystring');
var JUST = require('just');

var just = new JUST({root : __dirname + '/view', ext: '.html'});

var domain = 'leninlin.ru';
var port = '8880';

var googleOAuth = require('node-google-oauth')({
  googleClient: '329609228910-bpn2ejtp6dmqinvlcu5sg7jj0d40nhbo.apps.googleusercontent.com',
  googleSecret: '9LuEGkGy4zo4no_fi1RZM_9K',
  baseURL: 'http://' + domain + ':' + port,
  loginURI: '/login',
  callbackURI: '/callback',
  scope: 'https://www.googleapis.com/auth/cloudprint' // optional, default scope is set to user
});

var token = {};
var options = {
  url: 'https://www.google.com/cloudprint',
  method: 'POST',
  headers: {
    'Content-type': 'application/json',
  }
};
var ticket = 0;

/*
var token = {
  "access_token" : "ya29.LQAo_qNxrixaIx8AAADVaWkDVF_yRA94g2WST312-i8McgUo9-E3GMsTvqBfqA",
  "token_type" : "Bearer",
  "expires_in" : 3600,
  "id_token" : "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFlMTE2NTIxMWY4N2QyM2RjMjgyZGNiMmJmNDY4Mzg1Zjc2YzBmZjkifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwic3ViIjoiMTA5NTUyNDQ2ODYxODM3MjUzMjgwIiwiYXpwIjoiMzI5NjA5MjI4OTEwLWQ3N2swNGZyNzVwMHA1cjFyYjY1cHE2YmIyZDFtaGJwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXRfaGFzaCI6Ik02Wkc2QkZFcFJKNGl2UEdSZnJwVlEiLCJhdWQiOiIzMjk2MDkyMjg5MTAtZDc3azA0ZnI3NXAwcDVyMXJiNjVwcTZiYjJkMW1oYnAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJpYXQiOjE0MDIzMDUwODQsImV4cCI6MTQwMjMwODk4NH0.kKu590U3UggkCH_-wOajHq3x96PPzxT9Za4ZkT2rl4IjxeZPvljFafTUAXO9I3EOa-Vv7cv8it_XDHq5-gXwni1I_gtdBkpyZw9A-yJl_DkaksfSELUu1PSZneEIOn0byAXEtWbu9W0j6OALJrXpsRFJEWl0vPG03XEn-6tqeMg",
  "refresh_token" : "1/BzdW06e44Jhwip_BZzTvE7H1xHtdzsrQBX3KIfqI-0I"
}
*/

require('http').createServer(function(req, res) {
  if (req.url.match(/login/)) return googleOAuth.login(req, res);
  if (req.url.match(/insta_log_in/)) return InstaApi.authorize(req, res);
  if (req.url.match(/callback/)) return googleOAuth.callback(req, res);
  if (req.url.match(/insta_call_back/)) return InstaApi.callback(req, res);
  if (req.url.match(/settings/)) return settings(req, res);
  if (req.url.match(/addJob/)) return addJob(req, res);
  if (req.url.match(/insta_subscriptions/)) return InstaApi.subscription(req, res);
  if (req.url.match(/list_subscriptions/)) return InstaApi.listSubscription(req, res);
  if (req.url.match(/remove_subscription/)) return InstaApi.removeSubscription(req, res);
  if (req.url.match(/last_geo_images/)) return InstaApi.lastGeoImages(req, res);
  if (req.url.match(/last_images/)) return InstaApi.lastImages(req, res);
  if (req.url.match(/search_images/)) return InstaApi.searchImages(req, res);
  if (req.url.match(/get_image_for_print/)) return showImage(req, res);
}).listen(port);

googleOAuth.on('error', function(err) {
  console.error('there was a login error', err);
});

googleOAuth.on('token', function(tokenResponse, serverResponse) {
  token = JSON.parse(tokenResponse);

  options.headers['Authorization'] = 'Bearer ' + token.access_token;

  serverResponse.writeHead(302, {
    'Location': '/insta_log_in/',
  });
  serverResponse.end();
});

settings = function(req, res) {
  PrintApi.getPrintersList(function(printersList) {
    var printers = {};
    for (i in printersList) {
      printers[printersList[i].id] = printersList[i];
    }

    if (req.method == 'POST') {
      var data = '';
      req.on('data', function(chunk) {
        data += chunk.toString();
      });
      
      req.on('end', function() {
        var body = qs.parse(data);
        if (body.printer) {
          PrintApi.setPrinter(printers[body.printer]);
        }

        if (body.tag || (body.lat && body.lng && body.radius)) {
          InstaApi.deleteAllSubscriptions();
        }

        if (body.tag) {
          InstaApi.tag = body.tag;
          InstaApi.savePrintedImagesByTag(body.tag);
          InstaApi.addTagSubscription(body.tag);
        }

        if (body.lat && body.lng && body.radius) {
          InstaApi.location = {
            'lat': body.lat,
            'lng': body.lng,
            'radius': body.radius,
          };
          InstaApi.addLocationSubscription(InstaApi.location);
          setTimeout(function() { InstaApi.savePrintedImagesByLocation(InstaApi.loaction); }, 1000);
        }

        var content = generateForm(printers);
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.end(content);
      });
    }  else {
      var content = generateForm(printers);
      res.writeHeader(200, {"Content-Type": "text/html"});
      res.end(content);
    }
  });
};

generateForm = function(printers) {
  var content = '<form method="POST">' +
    '<label>Prints</label><br/>';

  for (i in printers) {
    if (printers[i].id == PrintApi.printer.id) {
      content += '<input type="radio" name="printer" value="' + printers[i].id + '" checked /> ' + printers[i].name;
    } else {
      content += '<input type="radio" name="printer" value="' + printers[i].id + '" /> ' + printers[i].name;
    }
  }

  content += '<br /><label>Tag</label><input type="text" name="tag" value="' + InstaApi.tag + '" />';
  content += '<br /><br /><label>Location</label><br /><label>lat</label><input type="text" name="lat" value="'+(InstaApi.location.lat?InstaApi.location.lat:'')+'" /><label>lng</label><input type="text" name="lng" value="'+(InstaApi.location.lng?InstaApi.location.lng:'')+'" /><label>radius</label><input type="text" name="radius" value="'+(InstaApi.location.radius?InstaApi.location.radius:'')+'" />';

  content += '<br /><br /><input type="submit" value="Save" /></form>';

  return content;
};

addJob = function(req, res) {
  var content = '<form method="POST">' +
    '<label>url</label><br/><input type="text" name="job" />';

  content += '<input type="submit" value="Save" /></form>';

  if (req.method == 'POST') {
    var data = '';
    req.on('data', function(chunk) {
      data += chunk.toString();
    });
    
    req.on('end', function() {
      var body = qs.parse(data);
      PrintApi.addJob(body.job);

      res.writeHeader(200, {"Content-Type": "text/html"});
      res.end(content);
    });
  }  else {
    res.writeHeader(200, {"Content-Type": "text/html"});
    res.end(content);
  }
}

sendApiRequest = function(cmd, data, cb) {
  var params = {
    url: options.url + (cmd[0]=='/' ? cmd : '/'+cmd),
    method: options.method,
    headers: options.headers,
    form: data,
  };

  request(params, function(err, res, body) {
    try {
      cb(JSON.parse(body));
    } catch (e) {
      console.error(body);
      console.error('ERROR api: ', params);
    }
  });
}

PrintApi = {

  printer: {},

  setPrinter: function(printerObj) {
    this.printer = printerObj;
  },

  getPrintersList: function(cb) {
    sendApiRequest('search', {}, function(res) {
      cb(res.printers);
    });
  },

  addJob: function(url) {
    var params = {
      'printerid': this.printer.id,
      'title': 'Print image ' + url,
      'ticket': {
        "version": "1.0",
        "print": {
          "vendor_ticket_item": [],
          "page_orientation": {"type": "PORTRAIT"},
          "color": {"type": "STANDARD_MONOCHROME"},
          "copies": {"copies": 1},
          "fit_to_page": {"type": "NO_FITTING"},
          "margins": {"top_microns": 0, "right_microns": 0, "bottom_microns": 0, "left_microns": 0}
        }
      },
      'content': url,
      'contentType': 'url',
    };

    console.log(url);

    sendApiRequest('submit', params, function(res) {
      console.log(JSON.stringify(res));
    });
  },
};

sendInstaApiRequest = function(cmd, data, cb) {
  var params = {
    url: options.url + (cmd[0]=='/' ? cmd : '/'+cmd),
    method: options.method,
    headers: options.headers,
    form: data,
  };

  request(params, function(err, res) {
    cb(JSON.parse(res.body));
  });
}

InstaApi = {
  "client_id": "b641ff33bffd4c6192ac2df316af4189",
  "client_secret": "324f9c887dbd4c2495b507ac9b6d5ba7",
  "redirect_uri": 'http://' + domain +':' + port + '/insta_call_back',
  "scopes": 'relationships',
  "grant_type": "authorization_code",
  "callback_url": 'http://' + domain +':' + port + '/insta_subscriptions',
  'access_token': '',
  'tag': '',
  'location': {
    'lat': null,
    'lng': null,
    'radius': null,
  },
  'geo_id': null,

  'printedImages': [],

  authorize: function(req, res) {
    var _this = this;

    res.writeHead(302, {
      'Location': 'https://api.instagram.com/oauth/authorize/?client_id=' + _this.client_id + '&redirect_uri=' + _this.redirect_uri + '&response_type=code&scope=' + _this.scopes,
    });
    res.end();
  },

  callback: function(req, res) {
    var _this = this;
    var code = qs.parse(req.url.split("?")[1]).code;

    var params = {
      'url': 'https://api.instagram.com/oauth/access_token',
      'method': 'POST',
      'form': {
        'client_id': _this.client_id,
        'client_secret': _this.client_secret,
        'grant_type': _this.grant_type,
        'redirect_uri': _this.redirect_uri,
        'code': code,
      }
    }

    request(params, function(err, response, body) {
      InstaApi.access_token = JSON.parse(body).access_token;

      res.writeHead(302, {
        'Location': '/settings/',
      });
      res.end();
    });
  },

  listSubscription: function(req, res) {
    this.getListSubscriptions(function(list) {
      var content = '<ul>';

      for (i in list) {
        content += '<li><a href="/remove_subscription?id='+list[i].id+'">'+JSON.stringify(list[i])+'</a></li>';
      }

      content += '</ul>';

      res.writeHeader(200, {"Content-Type": "text/html"});
      res.end(content);
    });
  },

  removeSubscription: function(req, res) {
    var id = qs.parse(req.url.split("?")[1]).id;
    this.deleteSubscription(id);

    res.writeHead(302, {
      'Location': '/list_subscriptions/',
    });
    res.end();
  },

  lastImages: function(req, res) {
    var max_id = min_id = 0;
    var query = req.url.split("?");
    if (query && query[1]) {
      query = qs.parse(query[1]);
      if (query && query.max_id) {
        max_id = query.max_id;
      }
      if (query && query.min_id) {
        min_id = query.min_id;
      }
    }
    this.getImagesByTag(InstaApi.tag, min_id, max_id, function(list) {
      just.render('last_images', {list: list, domain: domain, port: port}, function(error, html) {
        console.error(error);
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.end(html);
      });
    });
  },

  searchImages: function(req, res) {
    var max_id = min_id = 0;
    var query = req.url.split("?");
    query = query && query[1] ? qs.parse(query[1]) : {};
    
    query.lat = query.lat ? query.lat : InstaApi.location.lat;
    query.lng = query.lng ? query.lng : InstaApi.location.lng;
    query.distance = query.distance ? query.distance : InstaApi.location.radius;
    query.min_timestamp = (query&&query.min_timestamp?query.min_timestamp:new Date().getTime()-60*60*24);
    query.max_timestamp = (query&&query.max_timestamp?query.max_timestamp:new Date().getTime());

    this.getSearchImages(query, function(list) {
      just.render('last_images', {list: list, domain: domain, port: port, next_min: (query.min_timestamp-60*60*24), next_max: (query.max_timestamp-60*60*24)}, function(error, html) {
        console.error(error);
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.end(html);
      });
    });
  },

  lastGeoImages: function(req, res) {
    var max_id = min_id = 0;
    var query = req.url.split("?");
    if (query && query[1]) {
      query = qs.parse(query[1]);
      if (query && query.max_id) {
        max_id = query.max_id;
      }
      if (query && query.min_id) {
        min_id = query.min_id;
      }
    }
    this.getImagesByLocation(function(list) {
      just.render('last_images', {list: list, domain: domain, port: port}, function(error, html) {
        console.error(error);
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.end(html);
      });
    });
  },

  subscription: function(req, res) {
    var params = qs.parse(req.url.split("?")[1]);
    var response = '';

    if (params['hub.challenge']) {
      response = params['hub.challenge'];
      res.end(response);
      return;
    }

    var data = '';
    req.on('data', function(chunk) {
      data += chunk.toString();
    });
    
    req.on('end', function() {
      console.log(data);
      data = JSON.parse(data)[0];

      if (data.object == 'tag') {
        InstaApi.getImagesByTag(InstaApi.tag, 0, 0, function(images) {
          for (i in images) {
            var printed = false;
            for (k in InstaApi.printedImages) {
              if (InstaApi.printedImages[k] == images[i].id) {
                printed = true;
              }
            }

            if (!printed) {
              PrintApi.addJob('http://' + domain + ':' + port + '/get_image_for_print?id=' + images[i].id);
              InstaApi.printedImages.push(images[i].id);
            }
          }
        });
      } else if (data.object == 'geography') {
        InstaApi.geo_id = data.object_id;
        InstaApi.getImagesByLocation(function(images) {
          for (i in images) {
            var printed = false;
            for (k in InstaApi.printedImages) {
              if (InstaApi.printedImages[k] == images[i].id) {
                printed = true;
              }
            }

            if (!printed) {
              PrintApi.addJob('http://' + domain + ':' + port + '/get_image_for_print?id=' + images[i].id);
              InstaApi.printedImages.push(images[i].id);
            }
          }
        });
      }

      res.writeHeader(200, {"Content-Type": "text/html"});
      res.end();
    });
  },

  addSubscription: function(object, paramsObj) {
    var _this = this;

    var params = {
      'url': 'https://api.instagram.com/v1/subscriptions/',
      'method': 'POST',
      'form': {
        'client_id': _this.client_id,
        'client_secret': _this.client_secret,
        'object': object,
        'aspect': 'media',
        'callback_url': _this.callback_url,
      },
    }

    for (i in paramsObj) {
      params.form[i] = paramsObj[i];
    };

    request(params, function(err, response) {
      if (object == 'geography') {
        InstaApi.geo_id = JSON.parse(response.body).data.object_id;
      }
      console.log(response.body);
    });
  },

  addTagSubscription: function(tag) {
    this.addSubscription('tag', {'object_id': tag});
  },

  addLocationSubscription: function(location) {
    this.addSubscription('geography', {'lat': location.lat, 'lng': location.lng, 'radius': location.radius});
  },

  getListSubscriptions: function(cb) {
    var _this = this;

    var params = {
      'url': 'https://api.instagram.com/v1/subscriptions/?client_id=' + _this.client_id + '&client_secret=' + _this.client_secret,
    };

    request(params, function(err, response, body) {
      console.log(body);
      cb(JSON.parse(body).data);
    });
  },

  deleteSubscription: function(id) {
    var _this = this;

    var params = {
      'url': 'https://api.instagram.com/v1/subscriptions/?client_id=' + _this.client_id + '&client_secret=' + _this.client_secret + '&id=' + id,
      'method': 'DELETE',
    };

    request(params, function(err, response, body) {
      console.log(body);
    });
  },

  deleteAllSubscriptions: function() {
    var _this = this;

    var params = {
      'url': 'https://api.instagram.com/v1/subscriptions/?client_id=' + _this.client_id + '&client_secret=' + _this.client_secret + '&object=all',
      'method': 'DELETE',
    };

    request(params, function(err, response, body) {});
  },

  apiRequest: function(url, cb, data, secure) {
    var _this = this;

    if (secure == null) {
      secure = true;
    }

    var str = '';
    if (data) {
      for (i in data) {
        str += i + '=' + data[i] + '&';
      }
    }

    if (secure) {
      var params = {
        'url': 'https://api.instagram.com/v1' + (url[0]=='/' ? url : '/'+url) + '?' + str + 'access_token=' + InstaApi.access_token,
      };
    } else {
      var params = {
        'url': 'https://api.instagram.com/v1' + (url[0]=='/' ? url : '/'+url) + '?' + str + 'client_id=' + InstaApi.client_id,
      };
    }

    console.log(params);

    request(params, function(err, response, body) {
      try {
        cb(JSON.parse(body));
      } catch (e) {
        console.error(body);
        console.error('ERROR api: ', params);
      }
      
    });
  },

  getImage: function(id, cb) {
    this.apiRequest('/media/'+id, function(media) {
      cb(media.data);
    });
  },

  getImagesByTag: function(tag, min_id, max_id, cb) {
    if (tag) {
      params = null;
      if (min_id) {
        params = params == null ? {} : params;
        params['min_id'] = min_id;
      }
      if (max_id) {
        params = params == null ? {} : params;
        params['count'] = max_id;
      }
      this.apiRequest('/tags/' + tag + '/media/recent', function(medias) {
        cb(medias.data);
      }, params);
    } else {
      cb([]);
    }
  },

  getImagesByLocation: function(cb) {
    /*this.apiRequest('/geographies/' + InstaApi.geo_id + '/media/recent', function(medias) {
      cb(medias.data);
    }, null, false);*/
    this.apiRequest('/media/search', function(medias) {
      cb(medias.data);
    }, {lat: InstaApi.location.lat, lng: InstaApi.location.lng, distance: InstaApi.location.radius});
  },

  getSearchImages: function(query, cb) {
    this.apiRequest('/media/search', function(medias) {
      cb(medias.data);
    }, query);
  },

  savePrintedImagesByTag: function(tag) {
    var _this = this;
    this.getImagesByTag(tag, 0, 0, function(images) {
      for (i in images) {
        _this.printedImages.push(images[i].id);
      }
    });
  },

  savePrintedImagesByLocation: function(location) {
    var _this = this;
    this.getImagesByLocation(function(images) {
      for (i in images) {
        _this.printedImages.push(images[i].id);
      }
    });
  },

  getLocation: function(id, cb) {
    this.apiRequest('/locations/'+id, function(location) {
      cb(location.data);
    });
  },

}

var showImage = function(req, res) {
  var params = qs.parse(req.url.split("?")[1]);

  InstaApi.getImage(params.id, function(image) {
    just.render('image', {image: image}, function(error, html) {
      console.error(error);
      res.writeHeader(200, {"Content-Type": "text/html"});
      res.end(html);
    });
  });
}